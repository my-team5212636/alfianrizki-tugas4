import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  Touchable,
} from 'react-native';
import React from 'react';

export default function Detail({navigation}) {
  return (
    <View style={{flex: 1}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <ImageBackground
          source={require('../assets/image/img_detailplace.png')}
          style={{
            width: Dimensions.get('window').width,
            height: 317,
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingHorizontal: 25,
              backgroundColor: 'rgba(52, 52, 52, 0.0)',
              marginTop: 20,
            }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image
                source={require('../assets/icon/ic_leftarrow_white.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                source={require('../assets/icon/ic_bag_white.png')}
                style={{width: 25, height: 25}}
              />
            </TouchableOpacity>
          </View>
        </ImageBackground>

        <View style={styles.content}>
          <Text style={styles.title}>Jack Repair Seturan</Text>
          <Image
            source={require('../assets/image/image_rating.png')}
            style={{width: 75, height: 14, marginBottom: 13}}
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 15,
            }}>
            <View style={{flexDirection: 'row'}}>
              <Image
                source={require('../assets/icon/ic_location.png')}
                style={{marginRight: 5}}
              />
              <Text style={{width: '70%', color: '#979797'}}>
                Jalan Affandi (Gejayan), No.15, Sleman Yogyakarta, 55384
              </Text>
            </View>
            <TouchableOpacity>
              <Text style={{color: '#3471CD', fontWeight: 'bold'}}>
                Lihat Maps
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View
              style={{
                backgroundColor: '#11A84E1F',
                width: 60,
                padding: 5,
                borderRadius: 20,
                alignItems: 'center',
                marginRight: 15,
              }}>
              <Text
                style={{fontWeight: 'bold', fontSize: 12, color: '#11A84E'}}>
                BUKA
              </Text>
            </View>
            <Text style={{color: 'black', fontWeight: 'bold'}}>
              09:00 - 21:00
            </Text>
          </View>
        </View>
        <View style={styles.content2}>
          <Text style={{color: 'black', fontSize: 18, marginBottom: 10}}>
            Deskripsi
          </Text>
          <Text style={{color: '#595959', fontSize: 16, marginBottom: 23}}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
            gravida mattis arcu interdum lectus egestas scelerisque. Blandit
            porttitor diam viverra amet nulla sodales aliquet est. Donec enim
            turpis rhoncus quis integer. Ullamcorper morbi donec tristique
            condimentum ornare imperdiet facilisi pretium molestie.
          </Text>
          <Text style={{color: 'black', fontSize: 18, marginBottom: 6}}>
            Range Biaya
          </Text>
          <Text style={{color: '#8D8D8D', fontSize: 16, marginBottom: 35}}>
            Rp 20.000 - 80.000
          </Text>
          <TouchableOpacity
            style={styles.button}
            onPress={() =>
              navigation.navigate('HomeNavigation', {screen: 'Form'})
            }>
            <Text style={styles.txtButton}>Repair Disini</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    width: '100%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 19,
    borderTopRightRadius: 19,
    paddingHorizontal: 20,
    paddingTop: 25,
    marginTop: -20,
    paddingBottom: 17,
    borderBottomColor: '#EEEEEE',
    borderBottomWidth: 1,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'black',
    marginBottom: 5,
  },
  content2: {
    width: '100%',
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    paddingVertical: 25,
  },
  button: {
    backgroundColor: '#BB2427',
    borderRadius: 10,
    paddingVertical: 15,
    alignItems: 'center',
  },
  txtButton: {
    color: 'white',
    fontSize: 20,
    fontWeight: '500',
  },
});
