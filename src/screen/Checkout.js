import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';

export default function Checkout() {
  const [payment, setPayment] = useState([
    {
      title: 'Bank Transfer',
      image: require('../assets/image/img_transfer.png'),
      isChoosen: false,
    },
    {
      title: 'OVO',
      image: require('../assets/image/img_ovo.png'),
      isChoosen: false,
    },
    {
      title: 'Kartu Kredit',
      image: require('../assets/image/img_credit.png'),
      isChoosen: false,
    },
  ]);

  const handleItemPress = item => {
    const updatedData = payment.map(x =>
      x.title === item.title
        ? {...x, isChoosen: !x.isChoosen}
        : {...x, isChoosen: false},
    );
    setPayment(updatedData);
  };

  return (
    <View style={styles.screen}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.segment}>
          <Text style={styles.segmentTitle}>Data Customer</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.text}>Agil Bani</Text>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 10,
                color: 'black',
                marginLeft: 10,
                fontWeight: '300',
              }}>
              (0813763476)
            </Text>
          </View>
          <Text style={styles.text}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={styles.text}>gantengdoang@dipanggang.com</Text>
        </View>
        <View style={styles.segment}>
          <Text style={styles.segmentTitle}>Alamat Outlet Tujuan</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.text}>Jack Repair - Seturan</Text>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 10,
                color: 'black',
                marginLeft: 10,
                fontWeight: '300',
              }}>
              (027-343457)
            </Text>
          </View>
          <Text style={styles.text}>Jl. Affandi No 18, Sleman, Yogyakarta</Text>
        </View>
        <View style={styles.segmentBarang}>
          <View style={{flexDirection: 'row'}}>
            <Image source={require('../assets/image/img_shoes_cart.png')} />
            <View style={{marginLeft: 13}}>
              <Text
                style={{color: 'black', fontWeight: '400', marginBottom: 10}}>
                New Balance - Pink Abu - 40
              </Text>
              <Text style={{color: '#737373', marginBottom: 10}}>
                Cuci Sepatu
              </Text>
              <Text style={{color: '#737373'}}>Note : -</Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 15,
            }}>
            <Text style={{color: 'black', fontSize: 20}}>1 Pasang</Text>
            <Text style={{color: 'black', fontSize: 20, fontWeight: '700'}}>
              @Rp 50.000
            </Text>
          </View>
        </View>
        <View style={styles.segmentPembayaran}>
          <Text style={styles.segmentTitle}>Rincian Pembayaran</Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.text2}>Cuci Sepatu</Text>
              <Text style={{color: '#FFC107', fontSize: 15, marginLeft: 15}}>
                x1 Pasang
              </Text>
            </View>
            <Text style={styles.text2}>Rp 30.000</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <Text style={styles.text2}>Biaya Antar</Text>
            <Text style={styles.text2}>Rp 3.000</Text>
          </View>
          <View style={styles.separator}></View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 10,
            }}>
            <Text style={styles.text2}>Total</Text>
            <Text style={{color: '#034262', fontSize: 15, fontWeight: '800'}}>
              Rp 33.000
            </Text>
          </View>
        </View>
        <View style={styles.segment}>
          <Text style={styles.segmentTitle}>Pilih Pembayaran</Text>
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={payment}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => handleItemPress(item)}>
                <View
                  style={{
                    alignItems: 'center',
                    paddingVertical: 15,
                    paddingHorizontal: 20,
                    backgroundColor: item.isChoosen
                      ? 'rgba(3, 66, 98, 0.16)'
                      : 'white',
                    borderColor: item.isChoosen ? '#034262' : '#E1E1E1',
                    borderWidth: 1,
                    marginHorizontal: 15,
                    borderRadius: 10,
                    height: 82,
                    width: 140,
                  }}>
                  {item.isChoosen ? (
                    <Image
                      source={require('../assets/icon/ic_checked.png')}
                      style={{position: 'absolute', top: 10, right: 10}}
                    />
                  ) : null}
                  <Image source={item.image} />
                  <Text style={{marginTop: 10}}>{item.title}</Text>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
        <View
          style={{
            backgroundColor: 'white',
            paddingHorizontal: 20,
            paddingVertical: 22,
          }}>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.txtButton}>Pesan Sekarang</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F6F8FF',
    paddingTop: 5,
    paddingBottom: 50,
  },
  screen: {
    flex: 1,
    backgroundColor: '#F6F8FF',
    paddingTop: 5,
    paddingBottom: 50,
  },
  segment: {
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 20,
    marginBottom: 12,
    width: '100%',
  },
  segmentTitle: {
    color: '#979797',
    fontSize: 18,
    marginBottom: 10,
  },
  text: {
    fontSize: 18,
    marginBottom: 10,
    color: 'black',
    fontWeight: '300',
  },
  segmentBarang: {
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 20,
    marginBottom: 50,
    width: '100%',
  },
  segmentPembayaran: {
    paddingHorizontal: 25,
    paddingTop: 10,
    paddingBottom: 15,
    backgroundColor: 'white',
    marginBottom: 12,
  },
  text2: {
    color: 'black',
    fontSize: 15,
  },
  separator: {
    borderBottomColor: '#EDEDED',
    borderWidth: 1,
  },
  button: {
    backgroundColor: '#BB2427',
    borderRadius: 10,
    paddingVertical: 15,
    alignItems: 'center',
    width: '100%',
    marginBottom: 20,
    alignSelf: 'center',
  },
  txtButton: {
    color: 'white',
    fontSize: 15,
    fontWeight: '500',
  },
});
