import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React from 'react';

export default function DetailTransaction({navigation}) {
  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <View style={styles.topContent}>
          <View style={{flexDirection: 'row', marginBottom: 50}}>
            <Text style={{marginRight: 10, color: '#BDBDBD'}}>
              20 Desember 2020
            </Text>
            <Text style={{color: '#BDBDBD'}}>09:00</Text>
          </View>
          <Text
            style={{
              color: 'black',
              fontSize: 40,
              fontWeight: 'bold',
            }}>
            CS122001
          </Text>
          <Text
            style={{
              color: 'black',
              fontSize: 15,
              fontWeight: '400',
              marginBottom: 40,
            }}>
            Kode Reservasi
          </Text>
          <Text style={{color: '#6F6F6F', fontSize: 17, fontWeight: '300'}}>
            Sebutkan Kode Reservasi saat
          </Text>
          <Text style={{color: '#6F6F6F', fontSize: 17, fontWeight: '300'}}>
            tiba di outlet
          </Text>
        </View>
        <View style={styles.bottomContent}>
          <Text style={styles.title}>Barang</Text>
          <View style={styles.content}>
            <Image source={require('../assets/image/img_shoes_cart.png')} />
            <View style={{marginLeft: 13}}>
              <Text
                style={{color: 'black', fontWeight: '400', marginBottom: 10}}>
                New Balance - Pink Abu - 40
              </Text>
              <Text style={{color: '#737373', marginBottom: 10}}>
                Cuci Sepatu
              </Text>
              <Text style={{color: '#737373'}}>Note : -</Text>
            </View>
          </View>
          <Text style={styles.title}>Status Pesanan</Text>
          <TouchableOpacity
            style={styles.content}
            onPress={() =>
              navigation.navigate('TransactionNavigation', {screen: 'Checkout'})
            }>
            <Image
              source={require('../assets/icon/ic_circle_red.png')}
              style={{marginRight: 10}}
            />
            <View style={{flexDirection: 'row'}}>
              <View style={{width: '85%'}}>
                <Text
                  style={{
                    color: 'black',
                    fontSize: 17,
                    fontWeight: '300',
                  }}>
                  Telah Reservasi
                </Text>
                <Text style={{color: '#A5A5A5', fontSize: 13}}>
                  20 Desember 2020
                </Text>
              </View>
              <Text style={{color: '#A5A5A5', fontSize: 13}}>09:00</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  topContent: {
    width: '100%',
    backgroundColor: 'white',
    paddingVertical: 20,
    alignItems: 'center',
  },
  bottomContent: {
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 11,
    paddingVertical: 18,
  },
  title: {
    color: 'black',
    fontWeight: '300',
    marginBottom: 15,
  },
  content: {
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 25,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    marginBottom: 30,
  },
});
