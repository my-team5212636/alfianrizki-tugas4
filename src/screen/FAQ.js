import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';

export default function FAQ() {
  const [active, setActive] = useState([
    {
      id: 1,
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isActive: false,
    },
    {
      id: 2,
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isActive: false,
    },
    {
      id: 3,
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isActive: false,
    },
    {
      id: 4,
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isActive: false,
    },
    {
      id: 5,
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isActive: false,
    },
    {
      id: 6,
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isActive: false,
    },
  ]);

  const handleItemPress = item => {
    const updatedData = active.map(x =>
      x.id === item.id ? {...x, isActive: !x.isActive} : x,
    );
    setActive(updatedData);
  };
  return (
    <View style={styles.screen}>
      <ScrollView>
        <FlatList
          data={active}
          renderItem={({item}) => (
            <View style={{marginBottom: 15}}>
              <TouchableOpacity
                style={styles.content}
                onPress={() => handleItemPress(item)}>
                <Text style={styles.title}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit?
                </Text>

                {item.isActive ? (
                  <Image source={require('../assets/icon/ic_arrow_up.png')} />
                ) : (
                  <Image source={require('../assets/icon/ic_arrow_down.png')} />
                )}
              </TouchableOpacity>
              {item.isActive ? (
                <View style={styles.dropView}>
                  <Text style={styles.dropText}>{item.text}</Text>
                </View>
              ) : null}
            </View>
          )}
        />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 7,
    paddingVertical: 30,
  },
  content: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingLeft: 14,
    paddingRight: 10,
    paddingVertical: 11,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    color: 'black',
    fontSize: 18,
  },
  dropView: {
    backgroundColor: 'white',
    paddingLeft: 14,
    paddingRight: 10,
    paddingVertical: 11,
  },
  dropText: {
    fontSize: 18,
  },
});
