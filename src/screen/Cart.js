import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

export default function Cart({navigation}) {
  return (
    <View style={styles.screen}>
      <TouchableOpacity
        style={styles.content}
        onPress={() => navigation.navigate('Summary')}>
        <Image source={require('../assets/image/img_shoes_cart.png')} />
        <View style={{marginLeft: 13}}>
          <Text style={{color: 'black', fontWeight: '400', marginBottom: 10}}>
            New Balance - Pink Abu - 40
          </Text>
          <Text style={{color: '#737373', marginBottom: 10}}>Cuci Sepatu</Text>
          <Text style={{color: '#737373'}}>Note : -</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          marginTop: 40,
          alignSelf: 'center',
          alignItems: 'center',
        }}>
        <Image source={require('../assets/icon/ic_plus.png')} />
        <Text style={{color: '#BB2427', marginLeft: 10, fontWeight: 'bold'}}>
          Tambah Barang
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('Summary')}>
        <Text style={styles.txtButton}>Selanjutnya</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F6F8FF',
    padding: 10,
    alignItems: 'center',
  },
  content: {
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 25,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  button: {
    backgroundColor: '#BB2427',
    borderRadius: 10,
    paddingVertical: 15,
    alignItems: 'center',
    position: 'absolute',
    bottom: 1,
    width: '100%',
    marginBottom: 20,
  },
  txtButton: {
    color: 'white',
    fontSize: 15,
    fontWeight: '500',
  },
});
