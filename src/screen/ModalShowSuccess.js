import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  Modal,
} from 'react-native';
import React from 'react';

export default function ModalShowSuccess({show, onClose, onClick}) {
  return (
    <Modal visible={show} transparent={true}>
      <View style={styles.screen}>
        <TouchableOpacity onPress={onClose}>
          <Image source={require('../assets/icon/ic_close.png')} />
        </TouchableOpacity>
        <View style={styles.content}>
          <Text
            style={{
              color: '#11A84E',
              fontSize: 20,
              fontWeight: 'bold',
              marginBottom: 30,
            }}>
            Reservasi Berhasil
          </Text>
          <Image source={require('../assets/icon/ic_checklist.png')} />
          <Text style={{color: 'black', marginTop: 30, fontSize: 20}}>
            Kami Telah Mengirimkan Kode
          </Text>
          <Text style={{color: 'black', fontSize: 20}}>
            Reservasi Ke Menu Transaksi
          </Text>
          <TouchableOpacity style={styles.button} onPress={onClick}>
            <Text style={styles.txtButton}>Lihat Kode Reservasi</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingTop: 25,
  },
  content: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    marginTop: 100,
  },
  button: {
    backgroundColor: '#BB2427',
    borderRadius: 10,
    paddingVertical: 15,
    alignItems: 'center',
    position: 'absolute',
    bottom: 1,
    width: '100%',
    marginBottom: 20,
    width: '90%',
    alignSelf: 'center',
  },
  txtButton: {
    color: 'white',
    fontSize: 15,
    fontWeight: '500',
  },
});
