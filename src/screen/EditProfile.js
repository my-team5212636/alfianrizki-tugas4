import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';

export default function EditProfile() {
  return (
    <View style={styles.screen}>
      <ScrollView>
        <Image
          source={require('../assets/image/img_profile.png')}
          style={{alignSelf: 'center', marginBottom: 15}}
        />
        <TouchableOpacity style={{flexDirection: 'row', alignSelf: 'center'}}>
          <Image source={require('../assets/icon/ic_edit.png')} />
          <Text
            style={{
              color: '#3A4BE0',
              fontSize: 20,
              fontWeight: '450',
              marginLeft: 10,
            }}>
            Edit Foto
          </Text>
        </TouchableOpacity>
        <Text
          style={{
            color: '#BB2427',
            fontSize: 15,
            fontWeight: '500',
            marginTop: 55,
            marginBottom: 10,
          }}>
          Nama
        </Text>
        <TextInput style={styles.inputText} value="Agil Bani" />
        <Text style={styles.titleInput}>Email</Text>
        <TextInput style={styles.inputText} value="gilagil@gmail.com" />
        <Text style={styles.titleInput}>No hp</Text>
        <TextInput style={styles.inputText} value="08124564879" />
        <TouchableOpacity style={styles.btnSimpan}>
          <Text style={{color: 'white', fontSize: 20, fontWeight: 'bold'}}>
            Simpan
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingTop: 30,
  },
  titleInput: {
    color: '#BB2427',
    fontSize: 15,
    fontWeight: '500',
    marginBottom: 10,
  },
  inputText: {
    backgroundColor: '#F6F8FF',
    paddingLeft: 12,
    paddingVertical: 10,
    borderRadius: 10,
    marginBottom: 25,
  },
  btnSimpan: {
    backgroundColor: '#BB2427',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 10,
    marginTop: 50,
    marginBottom: 20,
  },
});
