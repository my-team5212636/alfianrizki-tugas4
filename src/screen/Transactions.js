import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';

export default function Transactions({navigation}) {
  return (
    <TouchableOpacity
      style={styles.screen}
      onPress={() =>
        navigation.navigate('TransactionNavigation', {
          screen: 'DetailTransaction',
        })
      }>
      <View style={styles.item}>
        <View style={{flexDirection: 'row', marginBottom: 13}}>
          <Text>20 Desember 2020</Text>
          <Text style={{marginLeft: 10}}>09:00</Text>
        </View>
        <Text style={{color: 'black', fontSize: 15, marginBottom: 3}}>
          New Balance - Pink Abu - 40
        </Text>
        <Text
          style={{
            color: 'black',
            fontSize: 15,
            fontWeight: '300',
            marginBottom: 13,
          }}>
          Cuci Sepatu
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{color: 'black'}}>Kode Reservasi : </Text>
            <Text style={{color: 'black', fontWeight: 'bold'}}>CS201201</Text>
          </View>
          <View
            style={{
              backgroundColor: '#F29C1F29',
              borderRadius: 20,
              paddingHorizontal: 10,
              paddingVertical: 3,
            }}>
            <Text style={{color: '#FFC107'}}>Reserved</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F6F8FF',
    padding: 15,
  },
  item: {
    backgroundColor: 'white',
    paddingHorizontal: 12,
    paddingVertical: 18,
    borderRadius: 10,
  },
});
